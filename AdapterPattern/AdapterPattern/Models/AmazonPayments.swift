import Foundation

class AmazonPayments {
    // MARK: - Properties
    //#..............................
    var payments = 0.0
    //#..............................

    /*.................... Class Methods....................*/
    func paid(value: Double, currency: String) {
        //#..........
        payments += value
        print("*. Paid: \(currency) \(convertToCash(price: value)) via Amazon Payments")
    }

    func fulfilledTransactions() -> Double { payments }
}
