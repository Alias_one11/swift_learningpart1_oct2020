import Foundation

extension AmazonPayments: PaymentGateway {
    // MARK: - Computed Property
    //#..............................
    var totalPayment: Double {
        //#..........
        let total = self.payments
        print("""
              |<--::Received via Amazon::-->|
              *. Total Payment: \(currency(price: total))
              """)

        return total
    }
    //#..............................

    /*.................... Implementing Stubs ....................*/
    func receivePayment(amt: Double) {
        //#..........
        self.paid(value: amt, currency: "USD")
    }
}

//class AmazonPaymentsAdapter: PaymentGateway {
//    // MARK: - Properties
//    //#..............................
//    private let amazonPayments = AmazonPayments()
//
//    /*:-Computed Property____-*/
//    var totalPayment: Double {
//        //#..........
//        let total = amazonPayments.payments
//
//        print("""
//              |<--::Received via Amazon::-->|
//              *. Total Payment: \(currency(price: total))
//              """)
//
//        return total
//    }
//    //#..............................
//    /*................ Wrapper Method ................*/
//
//    func receivePayment(amt: Double) {
//        //#..........
//        amazonPayments.paid(value: amt, currency: "USD")
//    }
//}
