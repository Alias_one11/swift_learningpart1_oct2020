import Foundation

// # MARK: - Protocol
protocol PaymentGateway {
    //#..........
    func receivePayment(amt: Double)
    var totalPayment: Double { get }
}

class Paypal: PaymentGateway {
    // MARK: - Properties
    //#..............................
    private var total = 0.0

    /*:-__Computed Property__-*/
    var totalPayment: Double {
        //#..........
        print("""
              \n|<--::received via Paypal::-->|
              *. Total payments: \(convertToCash(price: total))
              """)
        return total
    }
    //#..............................

    /*.................... Class Methods ....................*/
    func receivePayment(amt: Double) { total += amt }

}/*__END OF PAYPAL__*/
