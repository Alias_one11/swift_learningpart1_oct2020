import Foundation

class Stripe: PaymentGateway {
    // MARK: - Properties
    //#..............................
    private var total = 0.0

    /*:-__Computed Property__-*/
    var totalPayment: Double {
        //#..........
        print("""
              \n|<--::received via Venmo::-->| 
              *. Total Payments: \(convertToCash(price: total))
              """)
        return total
    }
    //#..............................

    /*.................... Class Methods....................*/
    func receivePayment(amt: Double) { total += amt }

}
