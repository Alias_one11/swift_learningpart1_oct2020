import Foundation

func main() -> Void
{
    print("\n\n[ Adapter-Pattern ]")
    print("-----------------------------------------------------------")
    /// - ©Example #1
    let paypal = Paypal()
    paypal.receivePayment(amt: 100)
    paypal.receivePayment(amt: 200)
    paypal.receivePayment(amt: 499)

    let stripe = Stripe()
    stripe.receivePayment(amt: 5.99)
    stripe.receivePayment(amt: 25)
    stripe.receivePayment(amt: 9.99)

    //# - All transaction types
    var paymentGateway: [PaymentGateway] = [paypal, stripe]

    //# - Using our adapter class to adapt our
    // class that did not conform to our protocol
    let amazonPayments = AmazonPayments()
    amazonPayments.receivePayment(amt: 120)
    amazonPayments.receivePayment(amt: 74.99)

    /*-----------------------------------------------------------
      The adapter conforms to the PaymentGateway protocol; thus,
      we can use it together with the other conforming types:
      -----------------------------------------------------------*/
    paymentGateway.append(amazonPayments)
    allTransactions(paymentGateway: paymentGateway)
    //*©-----------------------------------------------------------©*/

    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()

internal func allTransactions(paymentGateway: [PaymentGateway]) {
    var total: Double = 0.0

    for gateway in paymentGateway {
        total += gateway.totalPayment
    }

    print("\n*. All Transactions: \(currency(price: total))")
}

