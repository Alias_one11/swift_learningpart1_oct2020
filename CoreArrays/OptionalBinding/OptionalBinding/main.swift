import Foundation

func main() -> Void
{
    divT("Unwrapping Optionals")
    print("-----------------------------------------------------------")
    /// - ©Example #1 Optional binding
    let itemGathered: String? = "Diamond Longsword"
    let isShopOpen: Bool? = true
    
    let blackSmithShop = ["Bottle": 10, "Shield": 15, "Ocarina": 100]
    
    let questDirectory = [
        //__________
        "Fetch Gemstones": [
            "Objective": "Retrieves 5 gemstones",
            "Secret": "Complete in under 5 minutes"
        ],
        "Defeat Big Boss": [
            "Objective": "Beat the boss",
            "Secret": "Win with 50% health"
        ]
    ]
    
    if let item = itemGathered {
        print("1. You found an [ \(item) ]")
        //__________
    } else { print("1. Sorry no item found...") }
    
    // - ©Unwrapping optionals on the same line
    if let shopOpen = isShopOpen,
       let searchItem = blackSmithShop["Shield"] {// Dictionary key
        //__________
        print("2. We're open [ \(shopOpen) ] & we have [ \(searchItem) ] items in stock!")
    } else {
        print("2. We are not open & do not have your item...")
    }
    
    // - ©[if the key excist]?[return its value]
    if let fetchGems = questDirectory["Fetch Gemstones"]?["Objective"] {
        print("3. Active quest: \(fetchGems)")
        //__________
    } else { print("3. That quest is longer available...") }
    
    //*©-----------------------------------------------------------©*/
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()
