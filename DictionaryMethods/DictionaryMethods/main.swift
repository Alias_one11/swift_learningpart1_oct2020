import Foundation

func main() -> Void
{
    divT("Dictionary Methods")
    print("-----------------------------------------------------------")
    
    /// - ©Example #1 Caching & overriding items
    //*©-----------------------------------------------------------©*/
    var playerStats: [String: Int] = [
        "HP": 100, "Attack": 35, "Gold": 29
    ]
    
    // - ©Initial dictionary keys:values
    print("©Initial dictionary keys:values--> \(playerStats)\n")
    
    // - .updateValue: Updates the value stored in the dictionary for the
    // - given key, or adds a new key-value pair if the key does not exist.
    guard let oldValue = playerStats.updateValue(30, forKey: "Attack") else { return }
    
    // - ©Old value
    print("1a. oldValue--> Attack: \(oldValue)")
    // - ©New value
    print("""
          1b. playerStats.updateValue(30, forKey: Attack)
              --> New Value: \(playerStats["Attack"] ?? 0)
          """)
    
    /// - ©Example #2 Caching & removing items
    //*©-----------------------------------------------------------©*/
    // - ©Removing an element from the dictionary
    // playerStats["Gold"] = nil
    guard let removedValue = playerStats.removeValue(forKey: "Gold") else { return }
    print("2a. removedValue--> Gold: \(removedValue)")
    
    print("""
          2b. playerStats.removeValue(forKey: Gold)
              --> Removed Value [Gold: 29] = \(String(describing: playerStats["Gold"]))
          """)
    
    /// - ©Example #3 Nested dictionaries
    //*©-----------------------------------------------------------©*/
    /// - ©NOTE!!!-->The value for each key is going to be a dictionary
    let questBoard: [String : [String : String]] = [
        //__________
        "Fetch Gemstones": ["Objective": "Retrieves 5 gemstones",
                            "Secret": "Complete in under 5 minutes"],
        "Defeat Big Boss": ["Objective": "Beat the boss",
                            "Secret": "Win with 50% health"]
    ]
    
    guard let gemstoneObjective = questBoard["Fetch Gemstones"]?["Objective"] else { return }
    print("""
          3a. questBoard["Fetch Gemstones"]?["Objective"]
              --> Objective: \(gemstoneObjective)
          """)
    
    
    print("-----------------------------------------------------------")
    print()
}
// RUNS-APP
main()
