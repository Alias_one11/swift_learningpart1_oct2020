import Foundation

func barcodeFunc(_ productBarcode: BarCode) {
    switch productBarcode {
    //..........
    case let .upc(numberSystem, manufacturer, productCode, check):
        print("2. UPC: \(numberSystem), \(manufacturer), \(productCode), \(check)")
    //..........
    case let .qrCode(productCode):
        print("2. QRCODE: \(productCode)")
    //..........
    }
}

//#.....................................................

@discardableResult func drinkFormatter(drink: Beverage) -> String {
    //..........
    var drinkName = ""
    
    switch drink {
    case let .water(size, temp): drinkName = "\(size) \(temp.rawValue) Water"
    //..........
    case .juice(let fruit): drinkName = "\(fruit) Juice"
    //..........
    case .cocoa(let temp):
        if (temp == .roomTemp || temp == .cold) {
            drinkName = "Chocolate Milk"
        } else { drinkName = "Hot Chocolate" }
    //..........
    case let .expresso(shots): drinkName = "\(shots) shots Expresso"
    }
    
    print("*. Drink: \(drinkName)")
    return drinkName
}

