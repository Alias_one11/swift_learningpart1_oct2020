import Foundation

func main() -> Void
{
    divT("Enums In Swift IOS")
    print("-----------------------------------------------------------")
    print("\n/// - ©Example #1")
    
    drinkFormatter(drink: .water(.large, .cold))
    drinkFormatter(drink: .juice("Beet"))
    drinkFormatter(drink: .expresso(5))
    
    //*©-----------------------------------------------------------©*/

    print("\n/// - ©Example #2")
    
    var productBarcode = BarCode.upc(8, 85909, 51226, 3)
    
    productBarcode = .qrCode("214097102375-57")
    
    print("1. \(productBarcode)")
    
    barcodeFunc(productBarcode)
    
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()
