import Foundation

enum Beverage {
    case water(Size, Temperature)
    case juice(String)
    case cocoa(Temperature)
    case expresso(Int)
}

enum Temperature: String {
    case cold = " Ice"
    case roomTemp = " Room Temperature"
    case warm = " Warm"
    case hot = " hot"
}

enum Size: String { case small, medium, large }
