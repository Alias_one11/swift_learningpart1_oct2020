//  MyEnum.swift
//  Enumerations

import Foundation

enum BarCode {
    case upc(Int, Int, Int, Int),
         qrCode(String)
    
}

enum JediMaster: String {
    case yoda = "Yoda"
    case maceWindu = "Mace Windu"
    case quiGonJinn = "Qui-Gon Jinn"
    case obiWanKenobi = "Obi-Wan Kenobi"
    case lukeSkywalker = "Luke Skywalker"
}

