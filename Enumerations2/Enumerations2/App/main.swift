import Foundation

func main() -> Void
{
    print("\n\n[ Enumerations Part #2 ]")
    print("-----------------------------------------------------------")
    /// - ©Example #1 nested enums
    let helmet = Character.Helmet.iron
    print("*. SELECTED HELMET: \(helmet)")
    
    let weapon = Character.Weapon.dagger
    print("*. SELECTED WEAPON: \(weapon)\n")
    
    let character1 = Character.warrior(.sword, .diamond)
    print(character1.getDescription())
    
    let character2 = Character.thief(weapon: .bow, helmet: .iron)
    print(character2.getDescription())
    
    print()
    //*©-----------------------------------------------------------©*/
    /// - ©Example #2 Struct which contain enums
    let character3 = CharacterStruct(type: .warrior, weapon: .sword)
    character3.toStr()
    
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()
