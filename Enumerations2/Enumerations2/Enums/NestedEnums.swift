import Foundation

enum Character {
    ///# ....... Parent Character Enum Cases .......
    case thief(weapon: Weapon, helmet: Helmet)//<-- Child enum types
    case warrior(_ weapon: Weapon, _ helmet: Helmet)//<-- Child enum types
        
    ///# ........... Enum Weapon ...........
    enum Weapon { case bow, sword, dagger }
    
    ///# ........... Enum Helmet ...........
    enum Helmet { case wooden, iron, diamond }
    
    func getDescription() -> String {
        //..........
        switch self {
        case let .thief(weapon: weapon, helmet: helmet):
            return "*. Thief chose: [ \(weapon) ] & [ \(helmet) ] helmet"
        case let .warrior(weapon, helmet):
            return "*. Warrior chose: [ \(weapon) ] & [ \(helmet) ] helmet"
        }
    }
    
}
