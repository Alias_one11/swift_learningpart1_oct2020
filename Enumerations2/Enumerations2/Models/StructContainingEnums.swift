import Foundation

struct CharacterStruct {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    let type: CharacterType
    let weapon: WeaponType
    //#..............................
    
    ///# ........... Enums inside a struct ...........
    enum CharacterType { case thief, warrior }
    
    enum WeaponType { case bow, sword, dagger }
    
    @discardableResult func toStr() -> String {
        //..........
        let msg = """
                  *. CHARACTER TYPE: \(type)
                  *. CHARACTER WEAPON: \(weapon)
                  """
        
        print(msg)
        return msg
    }
}
