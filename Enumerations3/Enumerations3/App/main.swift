import Foundation

func main() -> Void
{
    print("\n\n[ Protocol Oriented Enum ]")
    print("-----------------------------------------------------------")
    /// - ©Example #1 Playing a game
    var player = EPlayer.dead
    
    player.increaseHeart()
    print("*. Player Hearts: \(player.numberOfHearts)")
    
    player.increaseHeart()
    print("*. Player Hearts: \(player.numberOfHearts)")

    player.getAttacked()
    print("*. Player Hearts: \(player.numberOfHearts)")
    
    player.getAttacked()
    print("*. Player Hearts: \(player.numberOfHearts)")
    
    //*©-----------------------------------------------------------©*/
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()
