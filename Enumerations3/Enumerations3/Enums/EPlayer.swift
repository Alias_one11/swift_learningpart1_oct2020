import Foundation

/// ---------------------------------------------------------------
/// conforms to protocol LifeSpan. There are two cases. The object
/// can be either dead or alive. The alive case contains an
/// associated value called currentHeart. It also contains a
/// gettable computed property, numberOfHearts. numberOfHearts is
/// determined based on whether self is case or alive.
/// ---------------------------------------------------------------

enum EPlayer: ILifeSpan {
    case dead
    case alive(currentHeart: Int)
    
    // # Computed property
    var numberOfHearts: Int {
        //..........
        switch self {
        case .dead: return 0
        case let .alive(currentHeart: numberOfHearts): return numberOfHearts
        }
    }
    
    mutating func getAttacked() {
        //..........
        switch self {
        case .alive(let numberOfHearts):
            if numberOfHearts == 1 {
                self = .dead
            } else {
                self = .alive(currentHeart: numberOfHearts - 1)
            }
        //..........
        case .dead: break
        }
    }
    
    mutating func increaseHeart() {
        //..........
        switch self {
        case .dead:
            self = .alive(currentHeart: 1)
        case let .alive(numberOfHearts):
            self = .alive(currentHeart: numberOfHearts + 1)
        }
    }
    
}
