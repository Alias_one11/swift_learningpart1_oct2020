import Foundation


/// -------------------------------------------------------
/// A protocol called, LifeSpan. It contains one property,
/// numberOfHearts and two mutating functions which will
/// be used to increase/decrease a player's heart.
/// -------------------------------------------------------

protocol ILifeSpan {
    //..........
    var numberOfHearts: Int { get }
    //.......... heart -1 🖤
    mutating func getAttacked()
    //.......... heart +1 🖤
    mutating func increaseHeart()
}
