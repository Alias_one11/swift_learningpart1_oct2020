import Foundation

import SwiftUI

// # Protocol or Interface in other programming languages
protocol GroceryItem {
    //..........
    func cost() -> Float
    func description() -> String
}

struct GroceryStore {
   ///# ............... Class Methods ...............
    static func printReceipt(customersCart: Customer) {
        //# Prints receipt of transaction
        print("*. Customer receipt: \(customersCart.name)")
        var total: Float = 0
        
        customersCart.groceries.forEach { item in
            //..........
            print(item.description())
            total += item.cost()
            /// # IF CASTING RUN CODE BELOW
            /** ...................................................
                if let groceryItem = item as? GroceryItem {
                    // # Prints out the formatted list with
                    // # a huge help from the protocol
                    print(groceryItem.description())
                    total += groceryItem.cost()
                }
               ...................................................*/
        }
        //..........
        print("*. Total: [ \(convertToCash(arg: total)) ]")
    }
}/// # END GroceryStore
//#.....................................................

func convertToCash(arg: Float) -> String {
    //..........
    return String(format: "$%.02f", arg)
}

