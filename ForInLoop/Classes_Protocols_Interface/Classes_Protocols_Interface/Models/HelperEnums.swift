import Foundation

struct Customer {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    let name: String
    var groceries: [GroceryItem]
    //#..............................
}
//#.....................................................

struct Apple: GroceryItem {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    let name: String
    let price: Float
    //#..............................
    
    func cost() -> Float { return price }
    
    func description() -> String {
        //..........
        return "*. Item:: \(name) : \(price)"
    }
}

import SwiftUI

struct Beef: GroceryItem {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    let name: String
    let weight, pricePerPound: Float
    //#..............................
    
    func cost() -> Float { weight * pricePerPound }
    
    func description() -> String {
        //..........
        return "*. Item:: \(name) | Weight: \(weight) |" +
               " Price Per Pound: \(convertToCash(arg: pricePerPound))"
    }

}
