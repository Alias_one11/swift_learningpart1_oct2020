import Foundation

func main() -> Void
{
    print("\n\n[ Classes & Protocol/Interface ]")
    print("-----------------------------------------------------------")
    /// - ©Example #1
    let greenApple = Apple(name: "Green Apple", price: 1.99)
    let goldenApple = Apple(name: "Golden Apple", price: 4.99)
    let newYorkSteak = Beef(name: "New York Steak", weight: 2.5, pricePerPound: 9.99)
    
    let aliasShoppingCart = Customer(
        name: "Alias",
        groceries: [
            greenApple,
            goldenApple,
            newYorkSteak
        ])
    
    GroceryStore.printReceipt(customersCart: aliasShoppingCart)
    //*©-----------------------------------------------------------©*/
    
    /// - ©Example #1
    
    
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()
