import Foundation

struct Auctioneer: Subject {
    // MARK: - Properties
    //#..............................
    private var biddersList: [Bidder] = []
    private var currentBid: Float = 0
    private var auctionEnded: Bool = false
    private var reservePrice: Float = 0

    // # Computed Property
    var bid: Float {
        //#..........
        set(value) {
            if auctionEnded {
                print("\n*. Auction has ended! No more bids excepted...")
            } else if value > currentBid {
                print("\n*. New Bid: \(currency(price: Double(value))) accepted...")
                //#..........
                if value >= reservePrice {
                    print("*. Reserve price met! Auction ended.")
                    auctionEnded = true
                }
                currentBid = value
            }
        }
        get { currentBid }
    }
    //#..............................

    /*.................... Initializer ....................*/
    // # An initializer that lets us set the reserve
    // # price and optionally the initial bid, too
    init(initialBid: Float = 0, reservePrice: Float) {
        self.bid = initialBid
        self.reservePrice = reservePrice
    }

    /*.................... Implementing Stubs ....................*/
    // # MARK: - Appends new observers to the internal list
    mutating func attach(observer: Bidder) {
        //#..........
        biddersList.append(observer)
    }

    // # MARK: - Removes an observer from the bidders array.
    mutating func detach(observer: Bidder) {
        //#..........
        // # The Array doesn't have a method to identify and
        // remove a particular item. We rely on the filter()
        // method to return a new list that does not contain
        // the input argument.
        biddersList = biddersList.filter { $0.id != observer.id }
    }

    func notifyObservers() {
        //#..........
        // # If the reserve was met, we compose
        // a message, otherwise the message is nil.
        let msg: String? = bid > reservePrice
                ? "Reserve has been met, item sold" : nil

        // # The notification contains the bid & the message.
        let notification = BidNotification(bid: bid, msg: msg)
        // # Invoke the update(notification:) method on every subscribed observer:
        biddersList.forEach { $0.update(notification: notification) }
    }
}
