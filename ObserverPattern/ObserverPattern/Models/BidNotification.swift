import Foundation

struct BidNotification {
    // MARK: - Properties
    //#..............................
    var bid: Float
    var msg: String?
    //#..............................
}

