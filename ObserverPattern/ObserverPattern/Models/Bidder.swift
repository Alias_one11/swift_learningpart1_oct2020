import Foundation

/*----------------------------------------------------------------------------
  We define the concrete types next. The Bidder class has to conform to the
  Observer protocol, so we need to implement the update(notification:) method.
  ----------------------------------------------------------------------------*/

class Bidder: Observer {
    // MARK: - Computed Properties
    //#..............................
    var id: Int {
        ObjectIdentifier(self).hashValue
    }
    //#..............................

    /*.................... Class Methods ....................*/
    func update(notification: BidNotification) {
        //#..........
        print("""
              *. ID: \(id)
                 Bid: \(notification.bid)
                 Message: \(notification.msg ?? "")
              """)
    }
}
