import Foundation

/*----------------------------------------------------------------------------
  we define the Observer protocol; it has an update(notification:) method
  that takes an argument of type Notification. Notification is a placeholder
  type defined using the associatedtype keyword. The conforming classes
  will provide the concrete type.
  ----------------------------------------------------------------------------*/
protocol Observer {
    //#..........
    associatedtype Notification// Like a generic `T` type
    //#..........
    func update(notification: Notification)
}

/*----------------------------------------------------------------------------
  Define the Subject protocol next. The subject allows observers to register
  and unregister. So, let’s add the attach(observer:) and detach(observer:)
  methods. These methods will change the state of the instance, so both need
  to be mutating.
  ----------------------------------------------------------------------------*/
protocol Subject {
    //#..........
    associatedtype O: Observer
    //#..........
    mutating func attach(observer: O)
    mutating func detach(observer: O)
    /* The Subject protocol, also needs a method to notify the observers. */
    func notifyObservers()
}