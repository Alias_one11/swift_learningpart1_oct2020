import Foundation

func main() -> Void
{
    print("\n\n[ Observer Pattern ]")
    print("-----------------------------------------------------------")
    /// - ©Example 1
    var auctioneer = Auctioneer(reservePrice: 500)
    let abel = Bidder()
    let juels = Bidder()
    let bob = Bidder()
    let tom = Bidder()

    // # MARK: - Registering our bidders
    auctioneer.attach(observer: abel)
    auctioneer.attach(observer: juels)
    auctioneer.attach(observer: bob)
    auctioneer.attach(observer: tom)

    /*-----------------------------------------
      Whenever a new bid is set, the auctioneer
      should notify the observers.
      -----------------------------------------*/
    auctioneer.bid = 100
    auctioneer.bid = 150
    auctioneer.bid = 300
    auctioneer.bid = 550
    //*©-----------------------------------------------------------©*/

    print("-----------------------------------------------------------\n")
}

// RUNS-APP
main()