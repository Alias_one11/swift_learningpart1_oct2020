import Foundation

struct SwiftProgrammer: IPerson {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    var firstName: String
    var lastName: String
    var birthDate: Date
    var profession: String = ""
    //#..............................
    init(firstName: String, lastName: String, birthDate: Date) {
        self.firstName = firstName
        self.lastName = lastName
        self.birthDate = birthDate
    }
    
    
}
