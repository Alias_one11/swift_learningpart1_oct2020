import Foundation

/**....................................................................
   Even though no functionality is implemented in a protocol, they are still considered a full-fledged type in the Swift programming language, and can mostly be used like any other type. What this means is that we can use protocols as parameters or return types for a function. We can also use them as the type for variables, constants, and collections. Let's look at some examples of this. For the next few examples, we will use the following Person protocol”
   ....................................................................*/

protocol IPerson {
    //..........
    var firstName: String { get set }
    var lastName: String { get set }
    var birthDate: Date { get set }
    var profession: String { get }
    //..........
    init(firstName: String, lastName: String, birthDate: Date)
}
