import Foundation

func main() -> Void
{
    divT("Merchant Shop")
    print("-----------------------------------------------------------")
    /// - ©Setting up shop
    // - ©Array
    var shopItemsArray = ["Stone Shield", "Bronze Hammer", "Gold Helm"]
    
    // - ©Dictionary
    let shopItemDictionary = ["Stone Shield": 15, "Bronze Hammer": 25,
                              "Gold Helm": 35, "Long Sword": 100]
    
    /// - ©Using the contains/insert method
    //*©-----------------------------------------------------------©*/
    let longSwordInStock = shopItemsArray.contains("Diamond LongSword")
    print("1. shopItemsArray.contains(Diamond Long Sword): \(longSwordInStock)")
    
    shopItemsArray.insert("Diamond LongSword", at: 3)
    print("2. shopItemsArray.insert(Diamond LongSword, at: 3): \(shopItemsArray[3])")
    
    // - ©Creating a key from a value in our array,
    // - to get an item in our dictionary
    let selectedItem = shopItemsArray[2]// Gold Helm
    guard let selectedItemPrice = shopItemDictionary[selectedItem] else { return }
    print("3. Key shopItemDictionary[selectedItem]--> Gold Helm: \(selectedItemPrice)")
    
    /// - ©Using Sets
    //*©-----------------------------------------------------------©*/
    let fullArmor: Set = ["Diamond Helm", "Diamond Armor",
                          "Diamond Greaves", "Diamond Braces"]
    
    let currentArmor: Set = ["Diamond Helm", "Diamond Braces"]
    
    // - ©Subtract items in the Sets to see which items were missing in each set
    let missingPieces = fullArmor.subtracting(currentArmor)
    print("4. Set subtracting missingPieces: \(missingPieces.debugDescription)")
    
    /// - ©Using Tuples
    //*©-----------------------------------------------------------©*/
    var armorPiece = (name: "Diamond Brace", cost: 55, canEquip: true)
    print("5. armorPiece: \(armorPiece)")
    
    // - ©Changing the values in our armorPiece
    armorPiece.name = "Cannon Gun"
    armorPiece.cost = 78
    armorPiece.canEquip = true
    
    print("""
          6. New armorPiece.name: \(armorPiece.name)
             New armorPiece.cost: \(armorPiece.cost)
             New armorPiece.canEquip: \(armorPiece.canEquip)
          """)
    
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()
