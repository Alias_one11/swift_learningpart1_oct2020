import Foundation

/// - ©You use a set instead of an array when you need to test efficiently
/// - for membership and you aren’t concerned with the order of the elements
/// - in the collection, or when you need to ensure that each element appears
/// - only once in a collection.

func main() -> Void
{
    divT("Sets & Set Methods")
    print("-----------------------------------------------------------")
    
    /// - ©Example #1 Creating a set & other methods | Inserting & removing items
    //*©-----------------------------------------------------------©*/
    // - ©
    var activeQuests: Set<String> = [
        "Fetch Gemstones", "Big Boss",
        "The Undertaker", "Granny Needs Firewood"
    ]
    
    print("©Initial Set #1 values: \(activeQuests.debugDescription)\n")
    
    // - ©Adding a quest
    activeQuests.insert("Only The Strong")
    
    print("""
          1a. activeQuests.insert(Only The Strong):
              \(activeQuests.debugDescription)
          """)
    
    // - ©Removing an quest in the set
    activeQuests.remove("The Undertaker")
    
    print("""
          1b. activeQuests.remove("The UnderTaker"):
              \(activeQuests.debugDescription)
          """)
    
    // - ©Checking if the set contains a certain quest
    print("""
          1c. activeQuests.contains(Only The Strong):
              TRUE/FALSE--> \(activeQuests.contains("Only The Strong"))
          """)
    
    // - ©Sorting our `Set` in alphabetical order
    let activeQuestsSorted = activeQuests.sorted()
    print("1d. activeQuestsSorted: \(activeQuestsSorted.debugDescription)")
    
    /// - ©Example #2 Set methods & operations
    //*©-----------------------------------------------------------©*/
    print("-----------------------------------------------------------")

    let completedQuests: Set = ["Big Boss", "All-4-One", "The Hereafter"]
    print("©Initial Set #2 values: \(completedQuests.debugDescription)\n")
    
    // - ©Return a new set with all the common values of two sets
    let commonQuest = activeQuests.intersection(completedQuests)
    print("2a. activeQuests.intersection(completedQuests): \(commonQuest.debugDescription)")
    
    // - ©Returns all values that are not in both Sets
    let differentQuest = activeQuests.symmetricDifference(completedQuests)
    print("2b. activeQuests.symmetricDifference(completedQuests): \(differentQuest.debugDescription)")

    // - ©Return a union of both Sets
    let allQuest = activeQuests.union(completedQuests)
    print("2c. activeQuests.union(completedQuests): \(allQuest.debugDescription)")
    
    // - ©Subtracting two Sets
    let clippedQuest = activeQuests.subtracting(completedQuests)
    print("2d. activeQuests.subtracting(completedQuests): \(clippedQuest.debugDescription)")

    //*©-----------------------------------------------------------©*/
    
    print("-----------------------------------------------------------")
    print()
}
// RUNS-APP
main()
