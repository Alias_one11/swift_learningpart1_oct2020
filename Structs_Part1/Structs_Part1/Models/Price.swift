import Foundation

struct Price {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    var USD: Double
    var rateToCAD: Double
    //#..............................
    
    ///# ............... Computed Property ...............
    var canadianCurrency: String {
        //..........
        return currency(price: USD * rateToCAD)
    }
}
