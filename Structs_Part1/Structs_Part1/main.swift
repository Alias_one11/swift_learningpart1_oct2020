import Foundation

func main() -> Void
{
    print("\n\n[ Structures Part #1 ]")
    print("-----------------------------------------------------------")
    /// - ©Example #1
    
    let purchase = Item(name: "Lamp", price: 10.50)
    print("*. Item:: \(purchase.name)\n" +
          "   Price: \(currency(price: purchase.price))\n")
    //*©-----------------------------------------------------------©*/
    
    /// - ©Example #1
    
    let purchase2 = Price(USD: 11, rateToCAD: 1.29)
    print("*. USD PRICE: \(currency(price: purchase2.USD))\n" +
          "   Price in CAD: \(purchase2.canadianCurrency)")
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()

// MARK: -∂ Provides a formatted currency in dollars "$123.44"
func currency(price: Double) -> String {
    //__________
    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    /// # formatter.locale = NSLocale.currentLocale()
    /// # This is the defaultIn Swift 4, this ^ has
    /// # been renamed to simply NSLocale.current
    guard let result = formatter.string(from: NSNumber(value: price))
    else { return "" }
    
    return result
}
