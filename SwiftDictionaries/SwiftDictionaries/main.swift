import Foundation

func main() -> Void
{
    divT("Dictionaries In Swift IOS")
    print("-----------------------------------------------------------")
    
    /// - ©Example #1 Creating a dictionary
    //*©-----------------------------------------------------------©*/
    //var blackSmithShop: [String : Int] = [:]
    var blackSmithShop: [String: Int] = [
        "Bottle": 10, "Shield": 15, "Ocarina": 1000
    ]
    
    print("1a. blackSmithShop: [String: Int]: \(blackSmithShop)")
    
    /// - ©Example #2 Modifying & accessing the values
    //*©-----------------------------------------------------------©*/
    // var shieldPrice = blackSmithShop["Shield"]
    
    // - ©Subscripting through the dictionary key & changing its value
    blackSmithShop["Bottle"] = 11
    print("""
          2a. blackSmithShop[Bottle] was = 15
          2b. Now it is = Bottle: \(blackSmithShop["Bottle"] ?? 0)
          """)
    
    // - ©Adding a new item to the dictionary, which will be added unordered
    blackSmithShop["Towel"] = 1
    print("2c. blackSmithShop[Towel]: \(blackSmithShop["Towel"] ?? 0)")
    print("2d. Prints out unordered: \(blackSmithShop)")

    /// - ©Example #3 Accessing all keys and values
    //*©-----------------------------------------------------------©*/
    // - ©Sepearting the key & values from our dictionaries
    // - & storing them seperate in arrays
    let allKeys = [String](blackSmithShop.keys)
    arrayToString(arrayName: "3a. allKeys = [String](blackSmithShop.keys)",
                  array: allKeys)
    
    let allValues = [Int](blackSmithShop.values)
    print("3b. allValues = [Int](blackSmithShop.values): \(allValues)")
    
    
    print("-----------------------------------------------------------")
    print()
}
// RUNS-APP
main()
